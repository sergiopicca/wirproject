This is the final dataset, splitted into three, basically there is an extrafiled
that defines the class of which a particular tweet belongs to. I decided to collapse
some "tag" into one, such as:

- SportsTeam,SportsOrganization ---> Sports
- MusicRecording,Music,... ---> Music
- Book, BookSeries ---> Book

This will make the classification easier also for normal classifier I think.
I will update the classifier sooner and also the results.