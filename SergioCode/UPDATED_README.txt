README.txt

This is the code I've done today (2019-07-16), there are two python files,
- KBfunctions.py:
	it contains all the function that I used to invoke the googleAPI,
	those functions are carefully described by some comments.
-KGSearch.py:
	just to try functions
	
-TypesExtraction.py:
    This is the code to label your data. What you need to do is to change the content of the variable "path", 
    by inserting the path of your dataset. Then you can start labeling, the advice is to not interrupt if you are not finished yet.
