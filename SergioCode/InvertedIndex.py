from KBfunctions import *
from KaiCode_preprocessing import *
import time
import json
import os
import random

dataset_paths = ["CSV/Sergio_one_label_data.csv","CSV/Gianluca_one_label_data.csv","CSV/Kai_one_label_data.csv"]
tweet_list = list()
lst = list()
set_of_entities = set()
pseudo_inverted_index = dict()

for path in dataset_paths:
    lst = readSingleLabeledCSV(path)
    tweet_list = tweet_list + lst

if not os.path.isfile("TXT/entity_file.txt"):
    for t in tweet_list:
        text = t.get('full_text')
        entities = preprocessing(text)
        entity_file = open("TXT/entity_file.txt", 'a')
        for e in entities:
            entity_file.write(e + "\n")

        entity_file.close()

#This variable is for error handling, in the case the API does not work (503 error: service not avaible)
error = True
while(error):
    print("A new iteration begin (there was an error)...\n")
    time.sleep(5)

    #this is the file in which all the entities are stored
    #the usage of a file is requested to speed up the process
    entity_file = open("TXT/entity_file.txt", 'r')
    print("Reading entities...\n")
    all_entities = entity_file.readlines()

    #words for which the search in GKG does not work
    stopwords = ['Part','Man','Good','Back','YouTube','LIVE','House','People','Album','Way','Love','How','Life']
    for e in all_entities:
        formatted_line = e.replace('\n', '')
        if formatted_line not in stopwords:
            set_of_entities.add(formatted_line)
    entity_file.close()

    already_written = set()
    #the error variable is putted to false, hopefully it will not change
    error = False
    if os.path.isfile('JSON_index/new_inverted_index.json'):
        with open('JSON_index/new_inverted_index.json', 'r') as f:
            lines = f.readlines()
            f.close()

        #this file will contain all the entities that are already written in the inverted index
        formattedFile = open("JSON_index/already_written.json", 'w')

        linesNumber = 0

        print("Formatting the files...\n")
        #the last line is stripped out, because it may cause problem in reading the file
        print(len(lines[:-1]))
        for line in lines[:-1]:
            linesNumber = linesNumber + 1
            if linesNumber != len(lines[:-1]):
                formattedFile.write(line)
            else:
                line = line[:-2]
                formattedFile.write(line)

        formattedFile.write("\n}\n")
        formattedFile.close()

        #the 'already_written.json' once has been written is open to store in a set the entities already written
        with open('JSON_index/already_written.json', 'r') as f:
            data = json.load(f)
            for e in data.keys():
                already_written.add(e)
        f.close()

        #the index is re-written because IT HAS WELL FORMED IN ORDER TO ADD OTHER ENTITIES
        #let's suppose that the last line is:
        # "entity_name": [{"id": --> in that case the row is incomplete, so it has to be removed
        #so the file is overwritten.

        with open('JSON_index/new_inverted_index.json', 'w') as f:
            for line in lines[:-1]:
                f.write(line)
        f.close()

    print("The entities are: " + str(len(set_of_entities)))
    print("The ALREADY WRITTEN entities are: " + str(len(already_written)))
    print("Start creating the inverted index...\n")

    #we remove the entities already written
    set_of_entities = set_of_entities - already_written
    print(set_of_entities)
    print("You still have to add: " + str(len(set_of_entities)) + "\n")

    graffa = False
    if os.path.isfile('JSON_index/new_inverted_index.json'):
        graffa = True

    with open('JSON_index/new_inverted_index.json', 'a') as fp:
        if not graffa:
            fp.write("{\n")

        whichOne = 0
        for e in set_of_entities:
            whichOne = whichOne + 1
            print(str(whichOne) + " - " + "Entity: " + e)
            #time.sleep(5)
            try:
                nodes = createListOfNodes(e)
            except urllib.error.HTTPError as err:
                if err.code == 503:
                    error = True
                    break
                else:
                    raise
            print("...")
            print("...")
            nodes = sorted(nodes, key=lambda i: i['id'])

            if whichOne != len(set_of_entities):
                #we add the entity and its nodes to the json file, if it is not the last a comma is required
                fp.write("\"" + e + "\": " + json.dumps(nodes) + ",\n")
            else:
                #otherwise the comma is not needed
                fp.write("\"" + e + "\": " + json.dumps(nodes) + "\n")

            print("Added to the index.\n")
            #time.sleep(10)

        if not error:
            fp.write("}")

