import urllib.request
import urllib.parse
import json
import csv

'''
This function search for a particular node in the Google Knowledge Graph,
you need to specify:
- query: what you want to search
- limit: how many node you want
'''
def search(query):
    '''The api_key is the key necessary for getting the google api'''

    api_key = "AIzaSyBIdX6WY97QW1DZcjlZ6VskeVL7XC9qDY8"
    service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
    limit = 50
    params = {
        'query': query,
        'limit': limit,
        'indent': True,
        'key': api_key,
    }
    url = service_url + '?' + urllib.parse.urlencode(params)
    response = json.loads(urllib.request.urlopen(url).read())

    return response


'''
This function print the JSON file that you get from the request to GoogleAPI
'''
def printNodes(response):
    count = 0
    for element in response['itemListElement']:
        count = count + 1
        print("*" * 40)
        print("NodeID: " + element['result']['@id'])
        print("Name: " + element['result']['name'] + "\n")
        print("-" * 40)

        print("Type: [")
        for t in element['result']['@type']:
            print(t + ", ")
        print("]")
        print("-" * 40)

        if 'image' in element['result']:
            print("URL: " + element['result']['image']['url'])

        print("Score: " + str(element['resultScore']))
        print("*" * 40)
        print("\n")

    print(count)


'''
This function put the nodes in a list, where each element of the list is a dictionary with 
the following fields:
- id: the node id;
- name: the node name;
- types: the list of the types of the node;
- score: the score of the node;
'''
def createListOfNodes(query):
    nodes = search(query)
    listOfNodes = list()
    if 'itemListElement' not in nodes:
        return listOfNodes

    for element in nodes['itemListElement']:
        id = ""
        name = ""
        score = 0.00
        types = list()

        if '@id' in element['result']:
            id = element['result']['@id']

        if 'name' in element['result']:
            name = element['result']['name']

        if '@type' in element['result']:
            types = element['result']['@type']

        if 'resultScore' in element:
            score = element['resultScore']

        toInsert = {
            'id': id,
            'name': name,
            'types': types,
            'score': score
        }
        listOfNodes.append(toInsert)

    return listOfNodes

'''
This function returns as output the first node of the GKG for a certain query
'''
def takeTheFirstNode(query):
    api_key = "AIzaSyBIdX6WY97QW1DZcjlZ6VskeVL7XC9qDY8"
    service_url = 'https://kgsearch.googleapis.com/v1/entities:search'
    params = {
        'query': query,
        'limit': 1,
        'indent': True,
        'key': api_key,
    }
    url = service_url + '?' + urllib.parse.urlencode(params)
    response = json.loads(urllib.request.urlopen(url).read())

    for element in response['itemListElement']:
        id = ""
        name = ""
        score = 0.00
        types = list()

        if '@id' in element['result']:
            id = element['result']['@id']

        if 'name' in element['result']:
            name = element['result']['name']

        if '@type' in element['result']:
            types = element['result']['@type']

        if 'resultScore' in element:
            score = element['resultScore']

        firstNode = {
            'id': id,
            'name': name,
            'types': types,
            'score': score
        }

        return firstNode

    firstNode = {
        'id': "",
        'name': "",
        'types': "",
        'score': ""
    }
    return  firstNode

'''
This function print the list of nodes created with the above function
'''
def printListOfNodes(listOfNodes):
    if not listOfNodes:
        print("empty list")

    for elem in listOfNodes:
        print(elem.get('id') + " - " + elem.get('name') + " - " + str(elem.get('score')) + " - " + ';'.join(
            x for x in elem.get('types')))
    print("\n #nodes: " + str(len(listOfNodes)) + "\n")

'''
This function creates the set of nodeIDs. The choice of creating a set is due to the fact
that with sets it is easier to see containment of one node into another lineage.
'''
def createIDSet(query):
    nodes = search(query)
    setOfIDs = set()

    if 'itemListElement' not in nodes:
        return setOfIDs

    for element in nodes['itemListElement']:
        id = ""

        if '@id' in element['result']:
            id = element['result']['@id']
            setOfIDs.add(id)

    return setOfIDs


def printSet(setOfIDs):
    if not setOfIDs:
        print("Empty set")

    count = 0
    for elem in setOfIDs:
        count = count + 1
        print(elem)

    print("#items: " + str(count))

'''
This function returns a list containing common nodes of two different lineages l1 and l2, represented
by two lists; then the new score is computed and it is obtained by adding the two score, s1 + s2. The 
formula on the paper does not apply since the scores are not percentage.
'''
def commonNodes(set_one, set_two, list_one, list_two):
    results = list()

    for elem in list_one:
        if elem.get('id') in set_one.intersection(set_two):
            id = elem.get('id')
            name = elem.get('name')
            types = elem.get('types')
            s1 = elem.get('score')

            for item in list_two:
                if item.get('id') in set_one.intersection(set_two) and item.get('id') == id:
                    s2 = item.get('score')
                    score = s1 + s2
                    toInsert = {
                        'id': id,
                        'name': name,
                        'types': types,
                        'score': score
                    }
                    results.append(toInsert)

    return results

'''
This function returns the set of tags that better describes the tweet, considering the two cases:
1 - there are common nodes of the lineages of the mentions;
2 - there are not common nodes.

In the first case we consider all the types of the common nodes once and we return them, avoiding the "Thing"-type.
In the second case we just pick the first node of the lineage, with higher score and we return the types of that node.
'''
def tweetTag(results,listOfEntites):
#I have two possible cases:
    #result is not empty
    if len(results) != 0:
        tags = set()
        for elem in results:
            for tag in elem.get('types'):
                tags.add(tag)

        tags.remove("Thing")
        return tags

    #result is empty so there are no nodes in common
    else:
        tags = set()
        for list in listOfEntites:
            for tag in list[0].get('types'):
                tags.add(tag)

        tags.remove("Thing")
        return tags

'''
This function takes as input a list of Entities and returns the list of entities with their own types
'''
def getTypes(listOfEntities):
    res = set()

    for entity in listOfEntities:
        elem = takeTheFirstNode(entity)
        types = elem.get("types")

        for t in types:
            res.add(t)

    return res

'''
This function allow you to read your csv file
'''
def readCSV(path):
    tuples = list()

    with open(path, 'rt')as f:
        data = csv.reader(f)
        for raw in data:
            id = raw[0]
            screen_name = raw[1]
            created_at = raw[2]
            full_text = raw[3]

            toInsert = {
                'id': id,
                'screen_name': screen_name,
                'created_at': created_at,
                'full_text': full_text
            }
            tuples.append(toInsert)

    del tuples[0]
    f.close()
    return tuples

'''
This function allow you to read your LABELED csv file
'''
def readLabeledCSV(path):
    tuples = list()

    with open(path, 'rt')as f:
        data = csv.reader(f, delimiter=',')
        for raw in data:
            id = raw[0]
            screen_name = raw[1]
            created_at = raw[2]
            full_text = raw[3]
            tag = raw[4]

            toInsert = {
                'id': id,
                'screen_name': screen_name,
                'created_at': created_at,
                'full_text': full_text,
                'tag': tag
            }
            tuples.append(toInsert)

    del tuples[0]
    f.close()
    return tuples

'''
This function write the new labeled dataset
'''
def writeNewCSV(path, listOfLabeled):
    file = path + "labeled_Sergio_data.csv"
    with open(file, 'w') as writeFile:
        writer = csv.writer(writeFile)
        fieldnames = ['id', 'screen_name', 'created_at','full_text','tag']
        writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
        writer.writeheader()
        for item in listOfLabeled:
            writer.writerow({'id': item.get('id'), 'screen_name': item.get('screen_name'), 'created_at': item.get('created_at'),
                             'full_text': item.get('full_text'),'tag': item.get('tag')})
    writeFile.close()

'''
This function write the new final labeled dataset
'''
def writeFinalCSV(path, listOfLabeled):
    file = path + "labeled_Sergio_data.csv"
    with open(file, 'w') as writeFile:
        writer = csv.writer(writeFile)
        fieldnames = ['id', 'screen_name', 'created_at','full_text','tag', 'repr_tag']
        writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
        writer.writeheader()
        for item in listOfLabeled:
            writer.writerow({'id': item.get('id'), 'screen_name': item.get('screen_name'), 'created_at': item.get('created_at'),
                             'full_text': item.get('full_text'),'tag': item.get('tag'), 'repr_tag': item.get('repr_tag')})
    writeFile.close()