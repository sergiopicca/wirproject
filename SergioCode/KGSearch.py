"""Example of Python client calling Knowledge Graph Search API."""
from KBfunctions import *

#Let's make and example with mentions that have common nodes in the lineages
query = "Mercedes"
query2 = "AMG"

#We create the lineages of the two mentions, with the nodes sorted in decreasing order of score
list_one = createListOfNodes(query)
list_two = createListOfNodes(query2)

#Then we create a set for each lineage. These sets will contain only the node ids, this is not
#required, is just a semplification in order to find in an easy way common nodes.
set_one = createIDSet(query)
set_two = createIDSet(query2)

printListOfNodes(list_one)
printListOfNodes(list_two)

print("This is the intersection of the two lineages:\n")
print(set_one.intersection(set_two))

#We look for common nodes in the two lineages
res = commonNodes(set_one,set_two,list_one,list_two)
printListOfNodes(res)

#We extract the types and then we output the tags for the tweets
emptyList = list()
tags = tweetTag(res, emptyList)
print("The possible tags for the tweet are:\n")
printSet(tags)
print("-"*80)

####################################################################
#Let's now try and example with two lineages with empty intersection
query = "Trump"
query2 = "AMG"

#We create the lineages of the two mentions, with the nodes sorted in decreasing order of score
list_one = createListOfNodes(query)
list_two = createListOfNodes(query2)

#Then we create a set for each lineage. These sets will contain only the node ids, this is not
#required, is just a semplification in order to find in an easy way common nodes.
set_one= createIDSet(query)
set_two = createIDSet(query2)

printListOfNodes(list_one)
printListOfNodes(list_two)

print("This is the intersection of the two lineages:\n")
print(set_one.intersection(set_two))

#We look for common nodes in the two lineages
res = commonNodes(set_one,set_two,list_one,list_two)
printListOfNodes(res)

#Since res is empty we have to create the list of lineages, in order to extract properly the tags
listOfLineages = list()
listOfLineages.append(list_one)
listOfLineages.append(list_two)

tags = tweetTag(res,listOfLineages)
print("The possible tags for the tweet are:\n")
printSet(tags)
print("-"*80)
