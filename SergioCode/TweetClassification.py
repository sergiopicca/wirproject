from KBfunctions import *
from KaiCode_preprocessing import *
from Classification_functions import *
from AuthorAndClasses import *
import itertools
import json
import sklearn
from sklearn.metrics import classification_report
import pandas

dataset_paths = ["CSV/Sergio_one_label_data.csv","CSV/Gianluca_one_label_data.csv","CSV/Kai_one_label_data.csv"]
inverted_index_path = "JSON_index/unpretty/inverted_index.json"
tweet_list = list()
classes = "classes.txt"
p = get_probabilities()

f = open(inverted_index_path,'r')
json_data = json.load(f)
f.close()

inverted_index = dict(json_data)


for path in dataset_paths:
    lst = readSingleLabeledCSV(path)
    tweet_list = tweet_list + lst

stopwords = "Part"      # We are removing this entity because the KB presents an error
y_true = list()         # Tweets classified by ourselves
y_pred = list()         # Tweets classified by KB
count = 0               # We store how many times occurred a "Type"

#target names will contain the names of the classes of the dataset
target_names = list()
less_tweet = tweet_list

f = open(classes,'r')
all_the_classes = f.readlines()
f.close()

for c in all_the_classes:
    if class_adjust(c.replace('\n','')) not in target_names:
        target_names.append(class_adjust(c.replace('\n','')))

print(target_names)

# error counters
one_node_error = 0      # counts the number of error done in the case of one node in common
more_node_error = 0     # counts the number of error done in the case of multiple nodes in common
no_node_error = 0       # counts the number of error done in the case of no node in common

# the "no_node" list is used to store the tweets for which the predicted tag is different from the real one,
# it is used only for the tweets that have no node in common.
no_node = list()

for tweet in less_tweet:
    print(tweet.get('id'))

    # we do preprocessing of the tweet by extracting entities
    entities = preprocessing(tweet.get('full_text'))
    author = tweet.get('screen_name')

    # remove the "stopwords" from the entities. The "stopwords" variable is only equal to "Part" since
    # it is the only entity for the Google Knowledge Graph API returns an error, in particular
    # 503: The service is currently unavailable. This happens every time we try to make a call to the API
    # by using "Part" as query, also in Google developer console.
    if stopwords in entities:
        entities.remove(stopwords)
    print(entities)
    common_nodes = list()

    print("-" * 40)
    print("Author: " + author + " | " + str(p.get(author)))
    print("-" * 40)

    if entities:
        #print(entities)
        y_true.append(class_adjust(tweet.get('single_tag')))


        # this for-loop allow us to scan the "posting lists" aka the lineages of the inverted index we built.
        for x, y in itertools.combinations(entities, 2):
            lst1 = inverted_index.get(x)
            lst2 = inverted_index.get(y)
            k = 0
            j = 0
            if lst1 and lst2:
                while k != len(lst1) and j != len(lst2):
                    if lst1[k].get('id') == lst2[j].get('id'):

                        # if we find the common nodes between entities.For each common node, we
                        # will save the "id", the "types" and we will sum the "score" of the nodes,
                        # by sum the ones of both lineages.

                        common_nodes.append(
                            (lst1[k].get('id'),
                             lst1[k].get('types'),
                             lst1[k].get('score') + lst2[j].get('score')))
                        k = k + 1
                        j = j + 1
                    else:
                        if lst1[k].get('id') < lst2[j].get('id'):
                            k = k + 1
                        else:
                            j = j + 1
        '''
        Now we have different cases, according to the length of common nodes:
        - Only one node is present.
        - More node is present, in this case the lineages of the entities intersect in multiple nodes.
        - No common nodes between lineages.
        '''
        if common_nodes:
            print("\nCOMMON NODES!\n")
            common_nodes = sorted(common_nodes, key=lambda i: i[2])

            if len(common_nodes) == 1:
                print("One node in common...")
                predicted_tag = class_adjust(one_node_types(common_nodes,target_names,author,p))
                print("Predicted tag: " + str(predicted_tag) + " | True tag: " + str(class_adjust(tweet.get('single_tag'))))
                y_pred.append(predicted_tag)
                if predicted_tag != class_adjust(tweet.get('single_tag')):
                    one_node_error+=1
            #if we have many nodes...
            else:
                print("More than one node in common...")
                predicted_tag = class_adjust(multiple_node_types(common_nodes,target_names,author,p))
                print("Predicted tag: " + str(predicted_tag) + " | True tag: " + str(class_adjust(tweet.get('single_tag'))))
                y_pred.append(predicted_tag)
                if predicted_tag != class_adjust(tweet.get('single_tag')):
                    more_node_error+=1
        else:
            print("No common nodes.")
            predicted_tag = class_adjust(no_common_nodes_types(entities, inverted_index, target_names,author,p))
            print("Predicted tag: " + str(predicted_tag) + "| True tag: " + str(class_adjust(tweet.get('single_tag'))))
            y_pred.append(predicted_tag)
            if predicted_tag != class_adjust(tweet.get('single_tag')):
                no_node_error += 1
                no_node.append((tweet.get('id'),predicted_tag,class_adjust(tweet.get('single_tag'))))

    print("-" * 40)
print(y_true)
print(y_pred)

if len(y_pred) == len(y_true):
    print("*" * 80)

    # print of metrics
    print("\n\nAccuracy score: ")
    print(sklearn.metrics.accuracy_score(y_true, y_pred))
    #print(len(y_true))
    #print(len(y_pred))
    #report = classification_report(y_true, y_pred,target_names=target_names,output_dict=True)

    # print of other metrics...
    report = classification_report(y_true, y_pred, target_names=target_names)
    print(report)
    #df = pandas.DataFrame(report).transpose()
    #where = "/Users/sergiopicca/Desktop/Sergio_Uni/Magistrale-Engineering_in_CS/WIR/Project/KBCode/RESULTS/report/"
    #export_csv = df.to_csv(where + 'export_dataframe.csv',index=True,header=True,sep=';')

else:
    print(len(y_true))
    print(len(y_pred))

print("Error on single node in common: " + str(one_node_error))
print("Error on multiple node in common: " + str(more_node_error))
print("Error on no node in common: " + str(no_node_error))
print(no_node)
