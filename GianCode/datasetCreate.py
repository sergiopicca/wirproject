"""
The following .py script creates the dataset by taking the tweets on Twitter.
You can give a sequence of queries to search and the program executes a Twitter's API call
for each query and it returns a json file where are stored all the information about the
tweet.
"""
import json
import urllib
import re
import emoji
from oauth import oauth

import hidden

'''
Given a tweet's text, it removes all the emoji
'''


def remove_emoji(text):
    chars_to_remove = ['\"', '\\', '/']
    text = text.translate(None, ''.join(chars_to_remove))
    return emoji.get_emoji_regexp().sub(u'', text)  # remove emoji


'''
Building the connection and make the request to Twitter API.
NB: YOU NEED THE KEYS TO MAKE IT. (I'VE BUILD 'hidden.py' WHERE I STORED MY KEYS).
'''


def augment(url, parameters):
    secrets = hidden.oauth()
    consumer = oauth.OAuthConsumer(secrets['consumer_key'], secrets['consumer_secret'])
    token = oauth.OAuthToken(secrets['token_key'], secrets['token_secret'])

    oauth_request = oauth.OAuthRequest.from_consumer_and_token(consumer,
                                                               token=token, http_method='GET', http_url=url,
                                                               parameters=parameters)
    oauth_request.sign_request(oauth.OAuthSignatureMethod_HMAC_SHA1(), consumer, token)
    return oauth_request.to_url()


"""
========================================================================================================================
Let's put inside the array 'search' all the terms that you want to find.
"""

'''
search = ['Harry Potter', 'League of Legends', 'Google', 'Spotify', 'Lord of the Rings', 'NASA', 'Oracle', 'High School', 'University',
          '4th of July', 'Burger King', 'Top Gun', 'The Dark Side of the Moon', 'AC/DC', 'New York', 'Chicago Bulls', 'Stranger Things',
          'La casa de papel', 'Crash Bandicoot', 'Spyro', 'Mercedes', 'Tom Cruise', 'NATO', 'Greenpeace', 'Uma Thurman',
          'Jennifer Lawrence', 'Juventus', 'Manchester City', 'Golden State', 'NBA', 'Marvel', 'Fox', 'Elon Musk', 'Leonardo DiCaprio',
          'Apple', 'Microsoft', 'Huawei', 'Amazon', 'Donald Trump', 'Ed Sheeran', 'Queen', 'The Lion King', 'Sony', 'EA', 'Western Stars',
          'Bruce Springsteen', 'Arctic Monkeys', 'Tim Cook', 'Steven Spielberg', 'Greta Thunberg', 'Star Wars', 'JJ Abrams', 'AMG',
          'Iron Man', 'Ferrari', 'F1', 'MotoGP']
'''
'''
search = ['neymarjr', 'realDonaldTrump', 'ArianaGrande', 'cnnbrk', 'NBA', 'Cristiano', 'BarackObama', 'amazon', 'charles_leclerc',
          'spacex', 'elonmusk', 'tesla', 'ChampionsLeague', 'BBCSport', 'BBCNews', 'Google', 'Ninja', 'Bwipo', 'G2Wunder',
          'premierleague', 'SkyNews']
'''
search = ['FortniteGame']
tweet = 0
for k in range(0, len(search)):                                    # Repeat all the following procedure for each term
    print('* Calling Twitter...')
    url = augment('https://api.twitter.com/1.1/search/tweets.json',
                  {'q': 'from:@' + search[k], 'count': '20', 'result_type': 'popular', 'tweet_mode': 'extended',
                   'lang': 'en'})

    # Inside URL, you will put all the parameters in which you are interested:
    # -- q:             is the term searched (in my case, I've added 'from:@' because I wanted all the tweets
    #                   written by a specific user, you can modify it as you like);
    # -- count:         limits the number of tweets;
    # -- result_type:   choose between 'popular', 'recent', 'mixed';
    # -- tweet_mode:    with this parameter, we have the COMPLETE text of the tweet, otherwise, limited by 120 chars;
    # -- lang:          only english tweets;

    # More details: https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html

    print(url)
    connection = urllib.urlopen(url)
    headers = connection.info().dict
    data = connection.read()
    jsonData = json.loads(data)
    # print(json.dumps(jsonData, indent=4))                         # <---- If you are interested on the output received

    print(search[k] + " --- " + str(len(jsonData['statuses'])))     # All the info that we need are inside 'statuses'
    lenJsonData = len(jsonData['statuses'])                         # Counting the tweets obtained by the query
    tweet = tweet + lenJsonData                                     # Adding to the total of tweets
    with open('dataset.json', 'w') as f:                            # Opening 'dataset.json' to write the tweets, where
                                                                    # for each tweet we will write the 'id', the 'user'
                                                                    # with the respectively name, the 'data' and the
                                                                    # 'text'
        if k == 0:
            f.write("{\n\t\"tweet\": [")
        for i in range(0, len(jsonData['statuses'])):

            if k == len(search) - 1 and i == len(jsonData['statuses']) - 1:
                full_text = remove_emoji(jsonData['statuses'][i]['full_text'].encode('ascii', 'ignore'))
                regex = re.compile(r'[\n\r\t]')
                strFull_text = regex.sub(" ", full_text)
                f.write(
                    "\n\t{\n\t\"id\":" + "\"" + jsonData['statuses'][i]['id_str'] + "\"" + "," +
                    "\t\"screen_name\":" + "\"" + jsonData['statuses'][i]['user']['screen_name'] + "\"" + "," +
                    "\t\"created_at\":" + "\"" + jsonData['statuses'][i]['created_at'] + "\"" + "," +
                    "\t\"full_text\":" + "\"" + strFull_text + "\"" + "}\n")
            else:
                full_text = remove_emoji(jsonData['statuses'][i]['full_text'].encode('ascii', 'ignore'))
                regex = re.compile(r'[\n\r\t]')
                strFull_text = regex.sub(" ", full_text)
                f.write(
                    "\n\t{\n\t\"id\":" + "\"" + jsonData['statuses'][i]['id_str'] + "\"" + "," +
                    "\t\"screen_name\":" + "\"" + jsonData['statuses'][i]['user']['screen_name'] + "\"" + "," +
                    "\t\"created_at\":" + "\"" + jsonData['statuses'][i]['created_at'] + "\"" + "," +
                    "\t\"full_text\":" + "\"" + strFull_text + "\"" + "},\n")

        if k == len(search) - 1:
            f.write("\t]\n}")
    f.close()
print("--- Total number tweets: " + str(tweet))                     # Printing how many tweets we have stored
