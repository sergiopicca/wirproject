#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 16:42:18 2019

@author: Kai
"""
import nltk
import string
import re
import pandas as pd
from nltk.stem import WordNetLemmatizer 

def language_detector(context):
    '''
    parameter:
        context: string
    return:
        Boolean
            True: it is English
            False: it is not English
    '''
    english_vocab = set(w.lower() for w in nltk.corpus.words.words())
    text_vocab = set(w.lower() for w in context if w.lower().isalpha())
    unusual = text_vocab.difference(english_vocab) 
    return len(unusual) == 0


def token_pos(context):
    '''
    parameter:
        context: string
    return:
        a list of tokens whose pos are noun
        Note: we are only interested in the tokens whose pos are Noun
            NN noun, singular ‘desk’
            NNS noun plural ‘desks’
            NNP proper noun, singular ‘Harrison’
            NNPS proper noun, plural ‘Americans’
    '''
    
    result = []
    noun = ['NN','NNS','NNP','NNPS']
    tokens = nltk.word_tokenize(context)
    pos = nltk.pos_tag(tokens)
    
    for token, p in pos:
        if p in noun:
            # add the lower case of each token
            result.append(token.lower())
            
    return result


def clean_str(text):
    text = text.lower()
    # Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"that's", "that is ", text)
    text = re.sub(r"there's", "there is ", text)
    text = re.sub(r"it's", "it is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "can not ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)

    return text.strip()

def lemmatize_data(input_data):
    # input is a list of string
    result = []
    wnl = WordNetLemmatizer()
    for token in input_data:
        result.append(wnl.lemmatize(token))
    return result 

def token_pos_nltk(text):
    word = nltk.word_tokenize(text)
    pos_tag = nltk.pos_tag(word)
    chunk = nltk.ne_chunk(pos_tag)
    NE = [ " ".join(w for w, t in ele) for ele in chunk if isinstance(ele, nltk.Tree)]
    return NE

def preprocessing(context):
    '''
    This is the main API in this module
    parameter:
        context: string
    return:
        a list of target word tokens
    '''
    if not language_detector(context):
        return None
    # clean the data
#    clean_context = ' '.join(lemmatize_data(clean_str(context).split()))

    # it seems it's better to do nother when call entity recognition api provided by nltk
    clean_context = context
    result = token_pos_nltk(clean_context)
    return result

def process(data_path):
    '''
    parameter:
        data_path: path for the csv data file
    return:
        list of set
        [{token_1_1, token_1_2},...{token_j_1, token_j_2}]
    '''
    result = []
    data = pd.read_csv(data_path)
    text_data = data['full_text'].values
    
    # process each Twitter and add the result
    for text in text_data:
        result.append(set(preprocessing(text)))

    return result

data_path = '../Dataset/Kai/datasetK.csv'
result = process(data_path)
#result = []
#data = pd.read_csv(data_path)
#text_data_1 = data['full_text'].values
#text_data = text_data_1
## process each Twitter and add the result
#for text in text_data:
#    result.append(preprocessing(text))
#data = process(data_path)
#context = 'Hello World, I want to swim'
#print(preprocessing(context))